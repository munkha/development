import React from "react"
import { Container, Card, ListGroup } from "react-bootstrap"
import { FaRegEnvelope, FaPhone } from "react-icons/fa"

const Contact = () => {
  return (
    <Container className="contactPage">
      <h1 className="m-3 title-section">Contact</h1>
      <Card style={{ width: "80%" }} className="mb-3">
        <ListGroup variant="flush">
          <ListGroup.Item>
            <FaPhone />
            720-648-5909
          </ListGroup.Item>
          <ListGroup.Item>
            <FaRegEnvelope />
            munkhsw@gmail.com
          </ListGroup.Item>
        </ListGroup>
      </Card>
    </Container>
  )
}

export default Contact
