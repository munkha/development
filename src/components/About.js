import React from "react"
import { Container, Row, Col, Card, Image, Button } from "react-bootstrap"
import Photo from "../images/About.jpg"

const About = () => {
  return (
    <Container>
        <h1 className="m-3 title-section">About</h1>
        <Row>
          <Col lg={6}>
            <Card>
              <Card.Body>
                <Image src={Photo} thumbnail />
              </Card.Body>
            </Card>
          </Col>
          <Col lg={6}>
            <Card>
              <Card.Body>
                My name is Munkh. I am software developer who has ability to
                play role at all phases of software development process.
              </Card.Body>
            </Card>
            <br/>
            <Card>
              <Card.Header><strong>Education</strong></Card.Header>
              <Card.Body>
                <Card.Title>
                  Mongolian University of Science and Technology
                </Card.Title>
                <Card.Text>B.S. in Software Engineering</Card.Text>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <Card.Title>Colorado Technical University</Card.Title>
                <Card.Text>M.S. in Software Engineering
                </Card.Text>
                <a href="https://docs.google.com/document/d/14wYaybyabT20aLAHZa3X2__SXh4mbmqo4hNMaqOlono/edit?usp=sharing"><Button variant="dark">Resume</Button></a>
                
              </Card.Body>
            </Card>
          </Col>
        </Row>
 
    </Container>
  )
}

export default About
