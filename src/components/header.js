import React from "react"
import { Link } from "gatsby"
import {Button} from "react-bootstrap"

const Header = () => {
  return (
    <div className="header-wrapper">
      <div className="main-info">
        <h1>Hi, I'm Munkh.</h1>
        <p>I am a software developer</p>
        <Link to="/contact" ><Button variant="dark">Contact Me</Button>
        </Link>
      </div>
    </div>
  )
}
export default Header
