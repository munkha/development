import React from "react"
import { FaFacebookSquare, FaLinkedin, FaTwitter, FaGit } from "react-icons/fa"
import { Container } from "react-bootstrap"

const Footer = () => {
  return (
    <Container fluid className="bg-dark ">
      <footer className="footer-container">
        <div className="social-wrapper">
          <div className="social-icons">
            <a href="https://www.facebook.com/munkh.amarsanaa">
              <FaFacebookSquare />
            </a>
            <a href="https://www.linkedin.com/in/munkh-amarsanaa/">
              <FaLinkedin />
            </a>
            <a href="https://twitter.com/munkhA29">
              <FaTwitter />
            </a>
            <a href="https://munkha@bitbucket.org/munkha">
              <FaGit />
            </a>
          </div>
          <div className="footer-bottom">
        <h4>Copyright&copy;{new Date().getFullYear()}. All rights reserved</h4>
        </div>
        </div>
      </footer>
    </Container>
  )

}
export default Footer
