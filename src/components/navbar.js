import { Link } from "gatsby"
import React from "react"
import { Container, Navbar, Nav } from "react-bootstrap"
import logo from "../images/logo.png"


const NavBar = () => (
    <Container fluid className="bg-dark">
    <Navbar expand="md" bg="dark" variant="dark">
      <Navbar.Brand><img src={logo} alt="logo"></img></Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarResponsive" />
      <Navbar.Collapse id="navbarResponsive" >
        <Nav as="ul" className="ml-auto">
          <Nav.Item as="li">
            <Link to="/" className="nav-link" activeClassName="active">Home</Link>
          </Nav.Item>
          <Nav.Item as="li">
            <Link to="/about" className="nav-link" activeClassName="active">About Me</Link>
          </Nav.Item>
          <Nav.Item as="li">
            <Link to="/experience" className="nav-link" activeClassName="active">Experience</Link>
          </Nav.Item>
          <Nav.Item as="li">
            <Link to="/skills" className="nav-link" activeClassName="active">Skills</Link>
          </Nav.Item>
          <Nav.Item as="li">
            <Link to="/contact" className="nav-link" activeClassName="active">Contact</Link>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
    </Container>
    

)


export default NavBar
