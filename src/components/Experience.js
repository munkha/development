import React from "react"
import { Container, Card } from "react-bootstrap"

const Experience = () => {
  return (
    <Container>
      <h1 className="m-3 title-section">Experience</h1>
      <Card style={{ width: "80%" }} className="mb-3">
        <Card.Body>
          <Card.Title>POSeLink, Inc</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">Software Developer</Card.Subtitle>
          <Card.Text>
            <ul>
              <li>Developed and improved some user interface using VB, C#, ASP.NET, ReactJS and Jquery.</li>
              <li>
               Improved reporting system performance using Oracle and MS SQL.
              </li>
              <li>
               Developed, executed, and interpreted load, stress and other perfromance tests.
              </li>
              <li>
               Identified, documented and reported bugs, errors and other issues.
              </li>
            </ul>
          </Card.Text>
        </Card.Body>
        <Card.Footer>Jan, 2020 - Apr, 2020 | Denver, Colorado</Card.Footer>
      </Card>
      <Card style={{ width: "80%" }} className="mb-3">
        <Card.Body>
          <Card.Title>Flint Software, LLC</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">Software Developer</Card.Subtitle>
          <Card.Text>
            <ul>
              <li>Designed and developed human resource management application, leveraging expertise in C#, ASP.NET
                MVC, Entity Framework, T-SQL and stored procedures.
              </li>
              <li>
               Developed and created time and attendance module using ASP.NET, Web API, C#, MS-SQL
              </li>
              <li>
               Implemented online recruiting and employee self-service module.
              </li>
              <li>
              Develope OLAP and OLTP reports based on busines needs using Crystal Reports and stored procedures.
              </li>
            </ul>
          </Card.Text>
        </Card.Body>
        <Card.Footer>Oct, 2009 - Sep, 2010 | Ulaanbaatar, Mongolia</Card.Footer>
      </Card>
    </Container>
  )
}

export default Experience
