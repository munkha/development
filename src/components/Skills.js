import React from "react"
import { Container, Card, Row, Col } from "react-bootstrap"
import html5 from "../images/html5.png"
import css3 from "../images/css3.png"
import javascript from "../images/javascript.png"
import bootstrap from "../images/bootstrap.png"
import react from "../images/react.png"
import nodejs from "../images/nodejs.png"
import visualstudio from "../images/visualstudio.png"
import github from "../images/github.png"
import sql from "../images/sql.png"
import data from "../images/data.png"
import jquery from "../images/jquery.png"
import csharp from "../images/csharp.png"

const Skills = () => {
  return (
    <Container>
      <h1 className="m-3 title-section">Skills</h1>
      <Card>
        <Card.Header>Web Development</Card.Header>
        <Card.Body className="skillSection">
          <Row>
            <Col sm={4} md={2}>
              <Card.Img src={html5} />
              <Card.Title>HTML 5</Card.Title>
            </Col>
            <Col sm={4} md={2}>
              <Card.Img src={css3} />
              <Card.Title>CSS3</Card.Title>
            </Col>
            <Col sm={4} md={2}>
              <Card.Img src={javascript} />
              <Card.Title>JS</Card.Title>
            </Col>
            <Col sm={4} md={2}>
              <Card.Img src={bootstrap} />
              <Card.Title>Bootstrap</Card.Title>
            </Col>
            <Col sm={4} md={2}>
              <Card.Img src={react} />
              <Card.Title>React</Card.Title>
            </Col>
            <Col sm={4} md={2}>
              <Card.Img src={nodejs} />
              <Card.Title>Node JS</Card.Title>
            </Col>
          </Row>
        </Card.Body>
      </Card>
      <Card>
        <Card.Header>Other</Card.Header>
        <Card.Body className="skillSection">
          <Row>
          <Col sm={4} md={2}>
              <Card.Img src={visualstudio} />
              <Card.Title>Visual Studio</Card.Title>
            </Col>
            <Col sm={4} md={2}>
            <Card.Img src={github} />
              <Card.Title>Github</Card.Title>
            </Col>
            <Col sm={4} md={2}>
            <Card.Img src={sql} />
              <Card.Title>SQL</Card.Title>
            </Col>
            <Col sm={4} md={2}>
            <Card.Img src={csharp} />
              <Card.Title>C#</Card.Title>
            </Col>
            <Col sm={4} md={2}>
            <Card.Img src={jquery} />
              <Card.Title>Jquery</Card.Title>
            </Col>
            <Col sm={4} md={2}>
            <Card.Img src={data} />
              <Card.Title>Data Structure</Card.Title>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Container>
  )
}

export default Skills
