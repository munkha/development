import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Skills from "../components/Skills"

const skillsPage = () => (
  <Layout>
    <SEO title="Skills" />
<Skills />
  </Layout>
)

export default skillsPage
