import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Experience from "../components/Experience"

const ExperiencePage = () => (
  <Layout>
    <SEO title="Experience" />
    <Experience />
  </Layout>
)

export default ExperiencePage
