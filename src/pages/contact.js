import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Contact from "../components/Contact"


const contactPage = () => (
  <Layout>
    <SEO title="Contacts" />
    <Contact />
  </Layout>
)

export default contactPage
